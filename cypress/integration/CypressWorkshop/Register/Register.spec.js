/// <reference types="cypress" />
let GLOBAL_EMAIL = "email"+Math.floor(Math.random()*10000)+"@gmail.com";
let GLOBAL_EMAIL1 = "bangush"+Math.floor(Math.random()*1000)+"@gmail.com";
let GLOBAL_EMAIL2 = "bahadur"+Math.floor(Math.random()*100)+"@gmail.com";

import DashboradLocators from "../../../fixtures/PageObjects/Dashboard.json"
import LoginLocators from "../../../fixtures/PageObjects/Login.json"
import RegisterLocators from "../../../fixtures/PageObjects/Register.json"


describe('Register Funtional Testing',()=>{

    it('tc 01 Verify user is able to access application resgistration page', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        
    })
    it('tc 03 Check all the radio buttons', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Checkbox_MaleGender).should('be.visible').click();
    })
    it('tc 04 Check the required fields by not entering any data', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Button_Register).should('be.visible').click();
        
    })
    it('tc 05 Check the required fields by  entering any data', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Input_FirstName).should('be.visible').type('tester');
        cy.get(RegisterLocators.Input_LastName).should('be.visible').type('tester');
        cy.get(LoginLocators.Input_Email).should('be.visible').type(GLOBAL_EMAIL);
        cy.get(LoginLocators.Input_password).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Input_ConfirmPassword).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Button_Register).should('be.visible').click();
        cy.get(LoginLocators.Username_On_Navbar).should('have.text',GLOBAL_EMAIL)
        
    })
    it('tc 06 Verfiy First Name Field Accepting Any Numeric data', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Input_FirstName).should('be.visible').type('123');
        cy.get(RegisterLocators.Input_LastName).should('be.visible').type('tester');
        cy.get(LoginLocators.Input_Email).should('be.visible').type(GLOBAL_EMAIL1);
        cy.get(LoginLocators.Input_password).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Input_ConfirmPassword).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Button_Register).should('be.visible').click();
        cy.get(LoginLocators.Username_On_Navbar).should('have.text',GLOBAL_EMAIL1)
        
    })
    it('tc 07 Verfiy First Name Field Accepting Any AlphaNumeric data', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Input_FirstName).should('be.visible').type('tester123');
        cy.get(RegisterLocators.Input_LastName).should('be.visible').type('tester');
        cy.get(LoginLocators.Input_Email).should('be.visible').type(GLOBAL_EMAIL2);
        cy.get(LoginLocators.Input_password).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Input_ConfirmPassword).should('be.visible').type('0123456789');
        cy.get(RegisterLocators.Button_Register).should('be.visible').click();
        cy.get(LoginLocators.Username_On_Navbar).should('have.text',GLOBAL_EMAIL2)
    
    })
    it('tc 08 Verfiy user is able to click on First name field', () => 
    {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_register).should('be.visible').click();
        cy.get(RegisterLocators.Input_FirstName).should('be.visible').click();

        
    })

})
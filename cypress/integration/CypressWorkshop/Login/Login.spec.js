/// <reference types="cypress" />
import DashboradLocators from "../../../fixtures/PageObjects/Dashboard.json"
import LoginLocators from "../../../fixtures/PageObjects/Login.json"

describe('Login Functionality',()=>{

    it('tc 02 Verify user is able to application login page', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        
          })

    it('tc03 Verify that as soon as login page opens, cursor by default should be on the email textbox ',() => {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible');
    })

    it('tc04 Verify that the user is able to login by entering registered Email in email textbox',() => {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('demo@cyz.com');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();
    })

    it('tc05 Verify that the user is able to login by entering registered password in password textbox.',() => {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_password).should('be.visible').type('1234567');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();
    })
    it('tc06 Verify that the user is able to login by clicking on login button.',() => {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('demo@cyz.com');
        cy.get(LoginLocators.Input_password).should('be.visible').type('1234567');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();
        cy.get(LoginLocators.Username_On_Navbar).should('have.text','demo@cyz.com');
    })
    
    it('tc07 Verify that the user is not able to login by entering unregistered Email in textbox.', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_password).should('be.visible').type('1234567');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();
        cy.get(LoginLocators.Verification_Message).should('have.text','Login was unsuccessful. Please correct the errors and try again.\nNo customer account found\n');
        
          })
           
    it('tc08 Verify that the user is not able to login by entering invalid password in textbox.', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('demo@cyz.com');
        cy.get(LoginLocators.Input_password).should('be.visible').type('12');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();
        cy.get(LoginLocators.Verification_Message).should('have.text','Login was unsuccessful. Please correct the errors and try again.\nThe credentials provided are incorrect\n');
        
          })
    it('tc09 Verify that the user is able to tick on remember me checkbox.', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('demo@cyz.com');
        cy.get(LoginLocators.Input_password).should('be.visible').type('1234567');
        cy.get(LoginLocators.CheckBox_RememberMe).should('be.visible').click();
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();   
        cy.get(LoginLocators.Username_On_Navbar).should('have.text','demo@cyz.com');     
          })
          
    it('tc10 Verify that the user is able to login without clicking on remember me  checkbox.', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('demo@cyz.com');
        cy.get(LoginLocators.Input_password).should('be.visible').type('1234567');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();   
        cy.get(LoginLocators.Username_On_Navbar).should('have.text','demo@cyz.com');     
          })
    it('tc11 Verify that the user is able to login without clicking on remember me  checkbox.', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('tester@gmail.com');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();   
        cy.get(LoginLocators.Verification_Message).should('have.text','Login was unsuccessful. Please correct the errors and try again.\nThe credentials provided are incorrect\n');

          })
    it('tc12 Verify that the user is not able to login without entering the email', () => {

        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        cy.get(LoginLocators.Input_Email).should('be.visible').type('tester@gmail.com');
        cy.get(LoginLocators.Form_Login_Button).should('be.visible').click();  
        cy.get(LoginLocators.Verification_Message).should('have.text','Login was unsuccessful. Please correct the errors and try again.\nThe credentials provided are incorrect\n');     
          })          


    
})

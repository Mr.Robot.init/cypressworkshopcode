/// <reference types="cypress" />
import DashboradLocators from "../../../fixtures/PageObjects/Dashboard.json"
describe('Dashboard Funtional Testing',()=>{

    it('tc 01 Verify that the dashboard opens', () => {
        cy.visit(DashboradLocators.url);
        cy.url().should('eq','https://demowebshop.tricentis.com/');
        
          })
    
    it('tc 02 Verify for the neccessary items on the homepage (example: register, login, add to cart etc)', () => {
        cy.visit(DashboradLocators.url);
        cy.get(DashboradLocators.button_login).should('be.visible').click();
        
          })      
    it('tc03 Verify user is able to access login page ',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.button_login).should('be.visible').click()
      cy.url().should('eq','https://demowebshop.tricentis.com/login');

    })

    it('tc04 Verify user is able to access register page ',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.button_register).should('be.visible').click()
      cy.url().should('eq','https://demowebshop.tricentis.com/register');

    })    
    it('tc05 Verify featured products are visible on dashboard',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.Product).should("be.visible")
    })  
    it('tc06 Verify add to cart button is clickable',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.Product).should('be.visible').click();
      cy.url().should('eq','https://demowebshop.tricentis.com/25-virtual-gift-card');
    })
    it('tc07 Verify product tiles are clickable',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.Product).should("be.visible").click() ;
      cy.url().should('eq','https://demowebshop.tricentis.com/25-virtual-gift-card');
    })
    it('tc08 Verify product tiles are clickable',()=>{
      cy.visit(DashboradLocators.url);
      cy.get(DashboradLocators.Product).should("be.visible").click() ;
      cy.get(DashboradLocators.Book_category).should('be.visible').click();
      cy.get(DashboradLocators.current_item).should('have.text','Books');
    })
    
   
})